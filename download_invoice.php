<?php
require_once 'vendor/autoload.php'; // Include Dompdf autoloader
use Dompdf\Dompdf;

require_once 'include/database.php';

// Fetch commands from database
$commandes = $pdo->query('SELECT commande.*, utilisateur.login as "login" FROM commande INNER JOIN utilisateur ON commande.id_client = utilisateur.id ORDER BY commande.date_creation DESC')->fetchAll(PDO::FETCH_ASSOC);

// Create an instance of Dompdf
$dompdf = new Dompdf();

// Load HTML content for invoice
$html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des Commandes</title>
    <style>
       
        body {
            font-family: Arial, sans-serif;
        }
        h2 {
            color: #333;
            font-size: 24px;
            margin-bottom: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #ccc;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
            font-weight: bold;
        }
        .btn {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 8px 14px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 4px;
        }
        .btn-success {
            background-color: #4CAF50;
        }
        .invoice-logo {
            max-width: 150px;
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
    <img src="/upload/produit/logo.jpg" alt="Logo" class="invoice-logo">


    <h2>Liste des Commandes</h2>
    <table>
        <thead>
            <tr>
                <th>#ID</th>
                <th>Client</th>
                <th>Total</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>';

foreach ($commandes as $commande) {
    $html .= '<tr>';
    $html .= '<td>' . $commande['id'] . '</td>';
    $html .= '<td>' . $commande['login'] . '</td>';
    $html .= '<td>' . $commande['total'] . ' <i class="fa fa-solid fa-dollar"></i></td>';
    $html .= '<td>' . $commande['date_creation'] . '</td>';
    $html .= '</tr>';
}

$html .= '</tbody>
    </table>
    
</body>
</html>';

// Load HTML into Dompdf
$dompdf->loadHtml($html);

// Set paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render PDF (optional: increase memory limit if necessary)
$dompdf->render();

// Output PDF
$dompdf->stream('liste_des_commandes.pdf');
?>
